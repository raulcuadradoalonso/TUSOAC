Event	ID	Name			Wwise Object Path	Notes
	235716118	DisableAerialEffect			\Default Work Unit\Sound FX\DisableAerialEffect	
	772106695	StopElevator			\Default Work Unit\Sound FX\Chapter 1\StopElevator	
	924933933	ApartmentOST			\Default Work Unit\OST\Chapter 1\ApartmentOST	
	1306411967	ElevatorMoves			\Default Work Unit\Sound FX\Chapter 1\ElevatorMoves	
	1404805401	DoorOpen			\Default Work Unit\Sound FX\Chapter 1\DoorOpen	
	1462315628	InGameMenuInactive			\Default Work Unit\InGameMenuInactive	
	1470211200	SomeoneKnocks			\Default Work Unit\OST\Chapter 1\SomeoneKnocks	
	1820391438	AerialCamera			\Default Work Unit\Sound FX\Chapter 1\AerialCamera	
	1940514798	StopEverything			\Default Work Unit\StopEverything	
	1995398023	OpenGates			\Default Work Unit\Sound FX\Chapter 1\OpenGates	
	2109880937	ElevatorClose			\Default Work Unit\Sound FX\Chapter 1\ElevatorClose	
	2155969695	TV_Sound			\Default Work Unit\Sound FX\Chapter 1\TV_Sound	
	2543937455	ElevatorDing			\Default Work Unit\Sound FX\Chapter 1\ElevatorDing	
	2554485435	ElevatorOpen			\Default Work Unit\Sound FX\Chapter 1\ElevatorOpen	
	2561485778	PressButton			\Default Work Unit\Sound FX\PressButton	
	2722794375	Rain_Sound			\Default Work Unit\Sound FX\Chapter 1\Rain_Sound	
	2850110183	KnockDoor			\Default Work Unit\Sound FX\Chapter 1\KnockDoor	
	2911132066	OpenFridge			\Default Work Unit\Sound FX\Chapter 1\OpenFridge	
	3174762640	ReleaseButton			\Default Work Unit\Sound FX\ReleaseButton	
	3211808491	LandCamera			\Default Work Unit\Sound FX\Chapter 1\LandCamera	
	3407863996	CloseFridge			\Default Work Unit\Sound FX\Chapter 1\CloseFridge	
	3561574529	InGameMenuActive			\Default Work Unit\InGameMenuActive	
	3644257135	StartDarkAmbience			\Default Work Unit\Sound FX\Chapter 1\StartDarkAmbience	

Effect plug-ins	ID	Name	Type				Notes
	1904434771	Medium_Room1	Wwise Matrix Reverb			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	93034938	RainLoop	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\RainLoop_F17FB28C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\RainLoop		12672064
	106208979	Elevator loop	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Elevator loop_F17FB28C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Elevator loop		1174208
	146540819	Be careful	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Be careful_F17FB28C.wem		\Actor-Mixer Hierarchy\Default Work Unit\OST\Be careful		21167832
	146861307	Fridge Close	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Fridge Close_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Fridge Close		44280
	232123968	Elevator arrives	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Elevator arrives_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Elevator arrives		264664
	264420945	Door knock	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Door knock_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Door knock		264664
	277885407	Fridge Open	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Fridge Open_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Fridge Open		44280
	383955182	Aerial Ambience	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Aerial Ambience_F17FB28C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Aerial Ambience		4586464
	535622833	Sitcom effect	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Sitcom effect_F17FB28C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Sitcom effect		36864064
	554813447	Character sound	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Character sound_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Character sound		26704
	848515385	Who knocks beta	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Who knocks beta_F17FB28C.wem		\Actor-Mixer Hierarchy\Default Work Unit\OST\Who knocks beta		26231560
	882965380	Elevator Door Open	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Elevator Door Open_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Elevator Door Open		475700
	922451784	PressButton	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\PressButton_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\PressButton		264664
	995331329	Door open	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Door open_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Door open		48252
	1017245178	Metal Gates	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\Metal Gates_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\Metal Gates		264664
	1018982240	ReleaseButton	D:\rca_r\Documents\1 - PROYECTOS\Unity\TUSOAC\TUSOAC_WwiseProject\.cache\Mac\SFX\ReleaseButton_1C9C67B2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sound FX\Chapter 1\ReleaseButton		264664

