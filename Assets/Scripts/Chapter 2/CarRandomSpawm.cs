﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class CarRandomSpawm : MonoBehaviour {

    [Header("Prefabs")]
    public GameObject[] Cars;
    [Header("Colors")]
    public Color[] Colors;
    [Header("SpawnPoints")]
    public GameObject LeftSpawnBack;
    public GameObject RightSpawnFront;
    public GameObject TargetSpawnBack;
    public GameObject TargetSpawnFront;
    [Header("Settings")]
    public float minTimeBetweenSpawns;
    public float maxTimeBetweenSpawns;
    public float minCarTimeSpeed;
    public float maxCarTimeSpeed;

    float timer = 0;
    float timeToSpawn;

    bool calculateTimeToSpawn = true;
    int numberOfCarsSpawned = 0;

    GameObject[] spawnPoints;

    void Start()
    {
        spawnPoints = new GameObject[2] { LeftSpawnBack, RightSpawnFront };
    }

    void Update ()
    {
        //Get the time to wait for the next spawn
        if (calculateTimeToSpawn)
        {
            timeToSpawn = Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns);
            calculateTimeToSpawn = false;
        }

        timer += Time.deltaTime;

        if (timer >= timeToSpawn)
        {
            timer = 0;
            calculateTimeToSpawn = true;
            //Spawn one or more cars
            StartCoroutine(spawnCar());
        }
	}

    IEnumerator spawnCar()
    {
        numberOfCarsSpawned++;

        //Choose spawn point
        int spawnPointIndex = Random.Range(0, 2);

        GameObject currentSpawnPoint = spawnPoints[0];
        GameObject targetSpawnPoint = TargetSpawnBack;

        if (spawnPointIndex != 0)
        {
            currentSpawnPoint = spawnPoints[spawnPointIndex];
            targetSpawnPoint = TargetSpawnFront;
        }

        //Get the car to be spawned
        GameObject carToSpawn = Cars[Random.Range(0, 3)];

        //Spawn car in that spawn point
        GameObject currentCar = Instantiate(carToSpawn, new Vector3 (currentSpawnPoint.transform.position.x, carToSpawn.transform.position.y, currentSpawnPoint.transform.position.z), 
            currentSpawnPoint.transform.rotation);

        //Get the speed of the car
        float currentSpeed = Random.Range(minCarTimeSpeed, maxCarTimeSpeed);

        //Move the car
        currentCar.Tween("Moving car: " + numberOfCarsSpawned, currentSpawnPoint.transform.position.x, targetSpawnPoint.transform.position.x, currentSpeed, TweenScaleFunctions.Linear,
            (t) => 
            {
                //Progress
                currentCar.transform.position = new Vector3 (t.CurrentValue, currentCar.transform.position.y, currentCar.transform.position.z);
            }, (t) =>
            {
                //Completion
                Destroy(currentCar.gameObject);
            } );

        yield return null;
    }
}
