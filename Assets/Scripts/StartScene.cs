﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScene : MonoBehaviour {

    public Image fader;

	// Use this for initialization
	void Start () {
        fader.gameObject.SetActive(true);
        fader.CrossFadeAlpha(0, 1.5f, true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
