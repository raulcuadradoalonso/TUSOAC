﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager instance;

    //[HideInInspector] public bool dictionariesLoaded;

    const string DEFAULT_DIALOGUES_PATH = "/Dialogues/";
    const string DEFAULT_ITEM_JSON_NAME = "_ItemsInteractions.json";
    const string DEFAULT_NPCS_JSON_NAME = "_NPCsInteractions.json";
    const string DEFAULT_NARRATOR_JSON_NAME = "_NarratorInteractions.json";
    const string DEFAULT_MAINMENU_JSON_NAME = "_MainMenu.json";

    [HideInInspector] public string LanguageSelected = "English"; //Default language

    Dictionary<string, string> itemsDictionary;
    Dictionary<string, string> npcsDictionary;
    Dictionary<string, string> narratorDictionary;
    Dictionary<string, string> mainmenuDictionary;

    string missingTextString = "Localized text not found";

    void Awake()
    {
        //First we check if LocalizationData has been already loaded
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //We check this again because Destroy method doesn't work inmediately (so this code could be executed again)
        if (instance == this)
        {
            DontDestroyOnLoad(gameObject);
            //Then we load the dictionaries using the Language Selected
            LoadDictionaries();
        }
    }

    public void LoadDictionaries()
    {
        itemsDictionary = LoadLocalizedText(LanguageSelected + DEFAULT_ITEM_JSON_NAME);
        narratorDictionary = LoadLocalizedText(LanguageSelected + DEFAULT_NARRATOR_JSON_NAME);
        npcsDictionary = LoadLocalizedText(LanguageSelected + DEFAULT_NPCS_JSON_NAME);
        mainmenuDictionary = LoadLocalizedText(LanguageSelected + DEFAULT_MAINMENU_JSON_NAME);
    }

    Dictionary<string, string> LoadLocalizedText(string fileName)
    {        
        Dictionary<string, string> localizedText = new Dictionary<string, string>();
        string filePath = Application.streamingAssetsPath + DEFAULT_DIALOGUES_PATH + fileName;
        //Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            //Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");            
        }
        else
        {
            Debug.LogError("Cannot find file!");
        }

        return localizedText;
    }

    public void SetLanguage(string language)
    {
        LanguageSelected = language;

        LoadDictionaries();
    }

    public Dictionary<string, string> GetItemsDictionary()
    {
        return itemsDictionary;
    }

    public Dictionary<string, string> GetNPCsDictionary()
    {
        return npcsDictionary;
    }

    public Dictionary<string, string> GetNarratorsDictionary()
    {
        return narratorDictionary;
    }

    public Dictionary<string, string> GetMainMenuDictionary()
    {
        return mainmenuDictionary;
    }
}

