﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarratorsTextState : MonoBehaviour
{

    //if an script is already updating alpha's value of this object, other scripts won't try to update it
    //instead they will wait to do it
    public bool isBeingUpdated = false;
}
