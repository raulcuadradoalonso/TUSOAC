﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ItemDialogTrigger : MonoBehaviour
{       
    [Header("Settings")]
    public float Y_OFFSET;
    public float X_OFFSET;
    public string Key;
    public Action afterInteractionFunction;

    [HideInInspector] public bool InteractionDone = false;
    [HideInInspector] public bool InPause = false;

    PlayerDialog pd;
    Image infoButton;

    bool enableInteraction = false;

    void Start ()
    {
        infoButton = GameObject.Find("InfoButton").GetComponent<Image>();
        infoButton.canvasRenderer.SetAlpha(0);

        pd = GameObject.Find("CubePlayer").GetComponent<PlayerDialog>();
	}

    void Update()
    {
        if (enableInteraction && !pd.IsInteracting && !InteractionDone)
        {
            showInfoButton();
            //Check if the player press the button
            if (Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Space)) //Button1 is B in Xbox Controllers
            {
                //Hide InfoButton
                infoButton.canvasRenderer.SetAlpha(0);
                //Start interaction
                if (pd.GetItemsDictionary().ContainsKey(Key))
                {
                    InteractionDone = true; //We use this for tracking items state in scripted moments
                    StartCoroutine(interaction());
                }
                else
                {
                    Debug.LogError("Cannot start Item Interaction, key not found.");
                }
            }
        }
    }
    

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInteraction = true;
        }            
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            infoButton.canvasRenderer.SetAlpha(0);
            enableInteraction = false;
        }        
    }

    void showInfoButton()
    {
        //Show InfoButton so the player knows that he must press it to interact
        infoButton.canvasRenderer.SetAlpha(1);
        infoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                        (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
    }

    IEnumerator interaction()
    {
        pd.StartInteraction(pd.GetItemsDictionary()[Key], pd.DialogueBox, pd.DialogueText);        

        while (pd.ShowingDialogue)
        {
            yield return null;
        }

        pd.EndOfConvesation();

        if (afterInteractionFunction != null)
        {
            afterInteractionFunction();
        }
    }
}
