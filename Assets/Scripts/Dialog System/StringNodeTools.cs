﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.UI;

public class StringNodeTools {    

    Font font;

    //Base size of the DialogBox image
    int DIALOG_BASE_HEIGHT;
    int DIALOG_BASE_WIDTH;
    //Base size of the DialogText 
    int TEXT_BASE_HEIGHT;
    int TEXT_BASE_WIDTH;
    //Best font values
    int DEFAULT_FONT_SIZE;
    int DEFAULT_SPACE_WIDTH;

    int DEFAULT_LINE_WIDTH;

    public StringNodeTools(int dialog_base_width, int dialog_base_height, int text_base_width, int text_base_height, int default_font_size, int default_space_width, Font font)
    {
        DIALOG_BASE_HEIGHT = dialog_base_height;
        DIALOG_BASE_WIDTH = dialog_base_width;

        TEXT_BASE_HEIGHT = text_base_height;
        TEXT_BASE_WIDTH = text_base_width;

        DEFAULT_FONT_SIZE = default_font_size;
        DEFAULT_SPACE_WIDTH = default_space_width;

        DEFAULT_LINE_WIDTH = TEXT_BASE_WIDTH;        

        this.font = font;
    }
    //This constructor is when we only want to use the split function
    public StringNodeTools(int default_space_width, Font font)
    {
        this.font = font;
        this.DEFAULT_SPACE_WIDTH = default_space_width;
    }

    public List<StringNode> splitText(string currentString, Text currentTextBox, float lineWidth = 0)
    {
        if (lineWidth == 0)
        {
            lineWidth = currentTextBox.rectTransform.rect.width;
        }

        StringBuilder sb = new StringBuilder();

        font.RequestCharactersInTexture(currentString, currentTextBox.fontSize);
        CharacterInfo characterInfo = new CharacterInfo();

        List<StringNode> stringNodes = new List<StringNode>();
        int currentStringWidth = 0;

        sb.AppendLine("Starting spit loop.");

        //Check if the string is larger than the box
        for (int i = 0; i < currentString.Length; i++)
        {
            font.GetCharacterInfo(currentString[i], out characterInfo, currentTextBox.fontSize);
            sb.AppendLine("Loop number: " + i);
            sb.AppendLine("Character: " + currentString[i]);
            sb.AppendLine(" - Current string width: " + currentStringWidth);

            //Space width it's not returned properly so we must take care of it manually
            int auxWidth = 0;

            if (currentString[i] != ' ')
            {
                auxWidth = characterInfo.advance;
            }
            else
            {
                auxWidth = DEFAULT_SPACE_WIDTH;
            }

            sb.AppendLine(" - Character glyph width: " + auxWidth);

            //Now we can start checking if the character fits into the current line
            if (currentStringWidth + auxWidth <= lineWidth)
            {
                currentStringWidth += auxWidth;
            }
            //If the character that does not fit is part of a word, we need to split the string before that word
            else if (currentString[i] != ' ' && currentString[i - 1] != ' ')
            {
                sb.AppendLine(" - The character doesn't fit and is part of a word.");
                //We make a trackback to find the last space and split there
                for (int j = i - 1; j > 0; j--)
                {
                    sb.AppendLine("    - Trackback looking for a space. Current character: " + currentString[j]);
                    font.GetCharacterInfo(currentString[j], out characterInfo, currentTextBox.fontSize);
                    sb.AppendLine("    - Updating current string width. Now: " + currentStringWidth);
                    currentStringWidth -= characterInfo.advance != ' ' ? characterInfo.advance : DEFAULT_SPACE_WIDTH; //This has not been debugged so it may cause problems
                    sb.AppendLine("    - After update: " + currentStringWidth);
                    if (currentString[j] == ' ')
                    {
                        sb.AppendLine("    - String split: " + currentString.Substring(0, j));
                        sb.AppendLine("    - String width: " + currentStringWidth);

                        StringNode sn = new StringNode(currentString.Substring(0, j), currentStringWidth);
                        stringNodes.Add(sn);
                        //We reset the for loop to begin after the split again                        
                        currentString = currentString.Substring(j + 1);
                        currentStringWidth = 0;
                        i = -1;
                        break;
                    }
                }
                continue;
            }
            //This means that we can split the line because the last character of the line is a space (so the next line will begin with a new word) 
            //We delete that space because we don't need it
            else
            {
                sb.AppendLine(" - The current character does not fit in the current line but it's not part of a word so we can split here.");
                //Using i instead of i + 1 removes the last space from the string
                StringNode sn = new StringNode(currentString.Substring(0, i), currentStringWidth);
                //So we need to update the current string width without the space
                currentStringWidth -= DEFAULT_SPACE_WIDTH;
                //We reset the for loop to begin after the split again
                if (i < currentString.Length - 1)
                {
                    sb.AppendLine(" - String split: " + currentString.Substring(0, i));
                    sb.AppendLine(" - String width: " + currentStringWidth);
                    //We need to chekc if the character that does not fit is an space (to remove it) or not
                    if (currentString[i] != ' ')
                    {
                        sb.AppendLine(" - Splitting. String remaining: " + currentString.Substring(i));
                        currentString = currentString.Substring(i);
                    }
                    else
                    {
                        sb.AppendLine(" - Splitting. String remaining: " + currentString.Substring(i + 1));
                        currentString = currentString.Substring(i + 1);
                    }
                    stringNodes.Add(sn);
                    //Reset the loop
                    i = -1;
                    currentStringWidth = 0;
                }
                continue;
            }

            if (i == currentString.Length - 1)
            {
                sb.AppendLine(" - String split: " + currentString.Substring(0, i + 1));
                sb.AppendLine(" - String width: " + currentStringWidth);

                sb.AppendLine(" - End of the loop.");
                //We check again if the last character is a space to remove it
                int auxLastIndex = i + 1;
                if (currentString[i] == ' ')
                {
                    auxLastIndex = i;
                    currentStringWidth -= DEFAULT_SPACE_WIDTH;
                }
                StringNode sn = new StringNode(currentString.Substring(0, auxLastIndex), currentStringWidth);
                stringNodes.Add(sn);
            }
        }
        //Debug.Log(sb.ToString());
        //Debug.Log(stringNodes.Count);
        return stringNodes;
    }

    public int getTextBoxWidth(List<StringNode> stringNodes)
    {
		//SE PODRÍA SIMPLEMENTE TENER EN CUENTA EL NÚMERO DE STRING NODES QUE HAY Y ESO DETERMINA EL NÚMERO DE LÍNEAS, CARAJO
        int totalStringWidth = 0;
        //First we get the total width of the dialogue
        for (int i = 0; i < stringNodes.Count; i++)
        {            
            totalStringWidth += stringNodes[i].stringWidth;
        }
        //Then we need to check the longest string to set the width and height values of the dialog box
        int aux_width = 0;
        //Minimun size, just one line
        if (totalStringWidth <= DEFAULT_LINE_WIDTH)
        {
            aux_width = stringNodes[0].stringWidth;
        }
        //Medium size, two lines of text
        else if (totalStringWidth <= DEFAULT_LINE_WIDTH * 2)
        {
            //We don't need to specify a sublist since we only have two lines            
            aux_width = findLongestString(stringNodes);
        }
        //Max size, three lines or more
        else
        {
            //Here we specify a sublist cause could be more than three lines (we are only interested in the three first lines now)
            aux_width = findLongestString(stringNodes.GetRange(0, 3));
        }

        return aux_width;
    }

    int findLongestString(List<StringNode> stringNodes)
    {
        int max = 0;
        for (int i = 0; i < stringNodes.Count; i++)
        {
            if (stringNodes[i].stringWidth > max)
            {
                //Debug.Log("Looking for longest string, now: " + stringNodes[i].stringNode);
                max = stringNodes[i].stringWidth;
            }
        }
        return max;
    }

    public float getEquivalentValue(float ref1, float ref2, float newRef1)
    {
        return (newRef1 * ref2) / ref1;
    }
}
