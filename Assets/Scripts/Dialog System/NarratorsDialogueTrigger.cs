﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class NarratorsDialogueTrigger : MonoBehaviour {

    public string Key;
    public float TimeActive; //When this time is reached, the text is hide   

    //Time for intros and outros
    const float transitionTime = 1f;
    //----------------IMPORTANT---------------------------------------------------------------------
    //THIS VALUE IS CALCULATED FOR THE CURRENT FONT SO IT COULD NOT BE VALID FOR OTHER FONTS
    //A FUNCTION THAT GETS THE VALUE IS NEEDED
	//CHANGIN THE SPLIT SYSTEM TO USE THE TEXTGENERATOR.GETPREFERREDWIDTH WOULD MAYBE SOLVE THIS PROBLEM
    const int DEFAULT_SPACE_WIDTH = 16; //Calculated for a fontsize of 60
    const int DEFAULT_FONT_SIZE = 60;

    //References
    PlayerDialog pd;
    Text narratorsText;
    StringNodeTools snt;
    //The text displayed by this trigger is split in different blocks that fit in two lines of text
    List<StringNode> narratorDialogues = new List<StringNode>();
    int dialoguesCounter;

    bool dialogueShowed = false;
    bool showingDialogue = false; //This is to track internally when to update the bool of NarratorsTextState
    bool enableTimer = false;
    float timer = 0;

	void Start ()
    {
        pd = GameObject.Find("CubePlayer").GetComponent<PlayerDialog>();
        narratorsText = GameObject.Find("NarratorsText").GetComponent<Text>();
        snt = new StringNodeTools(getSpaceWidth(), narratorsText.font);
        //Debug.Log(getSpaceWidth());

        dialoguesCounter = 0;

        if(pd.GetNarratorsDictionary().ContainsKey(Key))
        {
            splitText(pd.GetNarratorsDictionary()[Key]);
        }
        else
        {
            Debug.LogError("Cannot load narrator's dialogue, key not found.");
        }        
	}

    void Update()
    {
        if (enableTimer)
        {
            timer += Time.deltaTime;
            if (timer >= TimeActive && !narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated)
            {
                enableTimer = false;
                dialoguesCounter++;
                if (dialoguesCounter < narratorDialogues.Count)
                {
                    //Still some dialogues to show
                    hideDialogue(true);
                }
                else
                {
                    hideDialogue(false);
                }                
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !dialogueShowed)
        {
            dialogueShowed = true;

            StartCoroutine(manageDialogue());            
        }
    }

    IEnumerator manageDialogue()
    {
        showingDialogue = true;

        //Check if the text is being already updated for another trigger script (already performing an outro)
        if (!narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated)
        {
            chooseFunctionToCall();
        }
        //If so, we wait until it ends and then we show our dialogue
        else
        {
            while(narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated)
            {
                yield return null;
            }
            chooseFunctionToCall();
        }
    }

    void chooseFunctionToCall()
    {
        if (narratorsText.color.a == 0)
        {
            showDialogue();
        }
        else
        {
            hideDialogue(true);
        }
    }

    void hideDialogue(bool callShowDialogueAtTheEnd)
    {
        narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated = true;

        gameObject.Tween("Outro narrator", 1f, 0f, transitionTime, TweenScaleFunctions.Linear, (t) =>
        {
            //Progress
            narratorsText.color = new Color (narratorsText.color.r, narratorsText.color.g, narratorsText.color.b, t.CurrentValue);
        }, (t) =>
        {
            //Completion
            if (callShowDialogueAtTheEnd) { showDialogue(); }
            //If callShowDialogueAtTheEnd is false, means that this outro is the last function that is gonna be called in this 
            //script so we can destroy it for performance issues
            else
            {
                narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated = false;
                Destroy(this.gameObject);
            }
        });
    }

    void showDialogue()
    {
        if (narratorDialogues != null)
        {
            narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated = true;
            narratorsText.text = narratorDialogues[dialoguesCounter].stringNode;

            gameObject.Tween("Intro narrator", 0f, 1f, transitionTime, TweenScaleFunctions.Linear, (t) =>
            {
                //Progress
                narratorsText.color = new Color(narratorsText.color.r, narratorsText.color.g, narratorsText.color.b, t.CurrentValue);
            }, (t) =>
            {
                //Completion
                //We 'unbind' the text so another script could update it
                narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated = false;
                //Enable timer to perform an outro when TimeActive is reached
                enableTimer = true;
                timer = 0;
            });
        }
        else
        {
            Debug.LogError("Cannot set new narrator's dialogue. NarratorDialogues is null.");
            narratorsText.GetComponent<NarratorsTextState>().isBeingUpdated = false;
        }
    }

    void splitText(string auxText)
    {
        //We add *1.8f because we want to write 2 lines at the same time
        //Multiplying by 2 could lead to problems of space, maybe a line that has a width equal to width * 2 
        //doesn't fit because of spaces
        narratorDialogues = snt.splitText(auxText, narratorsText, narratorsText.rectTransform.rect.width * 1.8f);
        /*
        for (int i = 0; i < narratorDialogues.Count; i++)
        {
            Debug.Log(narratorDialogues[i].stringWidth);
        }
        */
    }

    int getSpaceWidth()
    {
        return (int)Mathf.Ceil(((float)narratorsText.fontSize * (float)DEFAULT_SPACE_WIDTH) / (float)DEFAULT_FONT_SIZE);
    }
}
