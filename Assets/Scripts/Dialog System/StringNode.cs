﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringNode {

    public string stringNode;
    public int stringWidth;

    public StringNode (string stringNode, int stringWidth)
    {
        this.stringNode = stringNode;
        this.stringWidth = stringWidth;
    }
}
