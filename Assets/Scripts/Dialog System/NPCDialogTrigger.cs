﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCDialogTrigger : MonoBehaviour {

    public NPCDialog[] Characters;
    [Header("Settings")]
    public float Y_OFFSET;
    public float X_OFFSET;
    public string[] Key;
    public float PauseTime;

    public bool DialogueFinished = false;
    public int GeneralDialoguesCounter = 0; //We use this to track the number of different conversations than an NPC can have 
    bool shouldUpdateGeneralCounter = true; //Maybe we want to keep a dialogue until the player does something, so we can decide when to update the counter

    int counter = 0; //We use this counter as the end of the key to advance in the dictionary
    Image infoButton;
    PlayerDialog pd;
    Dictionary<string, string> dictionary;

    bool enableInteracton = false;

    void Start()
    {
        infoButton = GameObject.Find("InfoButton").GetComponent<Image>();
        infoButton.canvasRenderer.SetAlpha(0);

        pd = GameObject.Find("CubePlayer").GetComponent<PlayerDialog>();
        dictionary = pd.GetDialogDictionary();
    }

    void Update()
    {
        if (enableInteracton && !pd.IsInteracting)
        {
            showInfoButton();
            //Check if the player press the button
            if ((Input.GetKeyDown(KeyCode.Joystick1Button1) || Input.GetKeyDown(KeyCode.Space))) //Button1 is B in Xbox Controllers
            {
                //Hide InfoButton
                infoButton.canvasRenderer.SetAlpha(0);
                //Start interaction
                StartCoroutine(startConversation());
                pd.IsInteracting = true; //Is interacting is set to false in playerdialog script for better management of the state
                //If an NPC can have different dialogues, this must be updated to true only when all the dialogues have been showed
                DialogueFinished = true;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInteracton = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            infoButton.canvasRenderer.SetAlpha(0);
            enableInteracton = false;
        }        
    }

    void showInfoButton()
    {
        //Show InfoButton so the player knows that he must press it to interact
        infoButton.canvasRenderer.SetAlpha(1);
        infoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                        (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
    }

    IEnumerator startConversation()
    {
        //Loop until we reach the end of the conversation
        while (dictionary.ContainsKey(Key[GeneralDialoguesCounter] + counter))
        {
            string next_dialogue = dictionary[Key[GeneralDialoguesCounter] + counter];
            //Pause command allow us to stop the conversation for a specified number of seconds
            //without giving back the control to the player.
            if (next_dialogue == "Pause")
            {
                yield return new WaitForSeconds(PauseTime);
            }
            //If it is not a Pause command, we must find the character that is going to talk
            else
            {
                Image aux_Dialogue_Box;
                Text aux_Dialogue_Text;
                //Special condition if that character is cube
                if (next_dialogue.Substring(0, 4) == "Cube")
                {
                    //Debug.Log("Sending dialogue to Cube");
                    aux_Dialogue_Box = pd.DialogueBox;
                    aux_Dialogue_Text = pd.DialogueText;
                }
                else
                {
                    //Debug.Log("Sending dialogue to an NPC");
                    aux_Dialogue_Box = findCharacter(next_dialogue).NPC_Dialog_Box;
                    aux_Dialogue_Text = findCharacter(next_dialogue).NPC_Dialog_Text;
                }
                //Send dialogue                
                pd.StartInteraction(splitDialogue(next_dialogue), aux_Dialogue_Box, aux_Dialogue_Text);
                //Wait until the dialogue is showed
                while(pd.ShowingDialogue)
                {
                    yield return null;
                }
            }
            counter++;
        }
        counter = 0;
        pd.EndOfConvesation();

        if (shouldUpdateGeneralCounter && GeneralDialoguesCounter < Key.Length - 1)
        {
            GeneralDialoguesCounter++;
        }

        yield return null;
    }

    NPCDialog findCharacter(string str)
    {
        string name = getCharacterName(str);

        for (int i = 0; i < Characters.Length; i++)
        {
            if (Characters[i].NPC_Name == name)
            {
                return Characters[i];
            }
        }

        return null;
    }

    string getCharacterName(string str)
    {
        for (int i = 0; i < str.Length; i++)
        {
            if (str[i] == ':')
            {
                return str.Substring(0, i);
            }
        }

        return "";
    }

    string splitDialogue(string str)
    {
        for (int i = 0; i < str.Length; i++)
        {
            if (str[i] == ':')
            {
                //i+2 is where the dialogue actually begins, the two values before are the : and the space characters
                //that we want to remove from the string
                return str.Substring(i + 2);
            }
        }

        return "";
    }
}
