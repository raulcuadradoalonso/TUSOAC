﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCDialog : MonoBehaviour
{
    [Header("Settings")]    
    public float X_OFFSET;
    public float Y_OFFSET;

    [HideInInspector] public Image NPC_Dialog_Box;
    [HideInInspector] public Text NPC_Dialog_Text;
    [HideInInspector] public string NPC_Name;

    void Awake()
    {
        NPC_Name = this.gameObject.name;
        NPC_Dialog_Box = GetComponentInChildren<Image>();
        NPC_Dialog_Text = GetComponentInChildren<Text>();

        NPC_Dialog_Box.canvasRenderer.SetAlpha(0);
        NPC_Dialog_Text.canvasRenderer.SetAlpha(0);
    }

    void Update()
    {
        NPC_Dialog_Box.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                      (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
        NPC_Dialog_Text.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                      (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
    }
}
