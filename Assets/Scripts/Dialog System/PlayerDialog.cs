﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class PlayerDialog : MonoBehaviour
{
    [Header("References")]
    public Camera cam;
    public Image DialogueBox;
    public Text DialogueText;
    public Font font;
    public GameObject playerCanvas;
    [Header("Position")]
    public float Y_OFFSET;
    public float X_OFFSET;
    [Header("Animation settings")]
    [Tooltip("For intros and outros.")]
    public float IOAnimationSpeed;
    [Tooltip("Value added incrementally to the scale in the animation.")]
    public float IOIncrement;
    [Tooltip("For transitions between different sizes.")]
    public float TAnimSpeed;
    [Tooltip("The target size is divided by this and those are the number of steps that will take the transition method.")]
    public float TAnimDivision;
    public float TypingSpeed;
    

    LocalizationManager _localizationManager;
    StringNodeTools _stringNodeTools;
    Image infoButton;

    //Base size of the DialogBox image
    const int DEFAULT_DIALOG_HEIGHT = 250;
    const int DEFAULT_DIALOG_WIDTH = 420;
    //Base size of the DialogText 
    const int DEFAULT_TEXT_HEIGHT = 140;
    const int DEFAULT_TEXT_WIDTH = 300;
    //Best font values
    const int DEFAULT_FONT_SIZE = 35;
    const int DEFAULT_SPACE_WIDTH = 9; //Calculated for a font size of 35.
    const int DEFAULT_NUMBER_OF_LINES = 3; //Lines per box

    float DEFAULT_X_OFFSET;
    float DEFAULT_Y_OFFSET;

    float fastTypingSpeed;
    //The text height will only be modified according to the current number of lines
    int currentHeight = DEFAULT_TEXT_HEIGHT;
    int currentNumberOfLines;

    //Dictionaries
    Dictionary<string, string> itemsDictionary;
    Dictionary<string, string> npcsDictionary;
    Dictionary<string, string> narratorDictionary;

    //Conditions    
    public bool ShowingDialogue = false;
    public bool IsInteracting = false;
    bool inTransition = false; //For smooth transitions, we have to wait while the transition is active

    Image auxDialogueBox;
    Text auxDialogueText;

    private void Awake()
    {
        playerCanvas.SetActive(true);
        //Setting default values
        DEFAULT_X_OFFSET = X_OFFSET;
        DEFAULT_Y_OFFSET = Y_OFFSET;
    }

    void Start()
    {
        StringBuilder sb = new StringBuilder();

        //Get dictionaries from LocalizationData class
        _localizationManager = GameObject.Find("LocalizationManager").GetComponent<LocalizationManager>();
        sb.AppendLine("Localization Manager found. ID of the instance: " + _localizationManager.GetInstanceID());
        itemsDictionary = _localizationManager.GetItemsDictionary();
        npcsDictionary = _localizationManager.GetNPCsDictionary();
        narratorDictionary = _localizationManager.GetNarratorsDictionary();
        sb.AppendLine(" - Items Dictionary loaded. Number of entries: " + itemsDictionary.Count);
        sb.AppendLine(" - NPCs Dictionary loaded. Number of entries: " + npcsDictionary.Count);
        sb.AppendLine(" - Narrator Dictionary loaded. Number of entries: " + narratorDictionary.Count);
        //Debug.Log(sb.ToString());

        //Setup of the dialogue system
        infoButton = GameObject.Find("InfoButtonDialogue").GetComponent<Image>();
        //----------------IMPORTANT---------------------------------------------------------------------
        //DIALOGUE SPACE WIDTH IS 9 BUT THIS SHOULD BE CALCULATED AT RUNTIME SO WE CAN CHANGE THE FONTSIZE IN THE EDITOR
        //WHITOUT SIZE PROBLEMS --- AND THIS VALUE IS CALCULATED FOR THE CURRENT FONT
        //A FUNCTION THAT GETS THE VALUE IS NEEDED
        //THIS COULD BE DONE USING A RULE OF THREE - IF A CHARACTER HAS X WIDTH WITH THIS FONT, IT WOULD HAVE Y WITH THE NEW FONT
        _stringNodeTools = new StringNodeTools(DEFAULT_DIALOG_WIDTH, DEFAULT_DIALOG_HEIGHT, DEFAULT_TEXT_WIDTH, DEFAULT_TEXT_HEIGHT, DEFAULT_FONT_SIZE, DEFAULT_SPACE_WIDTH, font);
        fastTypingSpeed = TypingSpeed * 0.5f;
        currentNumberOfLines = DEFAULT_NUMBER_OF_LINES;
        //Hide Canvas
        infoButton.canvasRenderer.SetAlpha(0);
        hideDialogue(DialogueBox, DialogueText);        
    }

    void Update()
    {
        DialogueBox.transform.position = cam.WorldToScreenPoint(new Vector3
                                      (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
        DialogueText.transform.position = cam.WorldToScreenPoint(new Vector3
                                      (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
    }

    void hideDialogue(Image currentDialogueBox, Text currentDialogueText)
    {
        currentDialogueBox.canvasRenderer.SetAlpha(0);
        currentDialogueText.canvasRenderer.SetAlpha(0);
        currentDialogueText.text = "";

        currentDialogueBox.rectTransform.sizeDelta = new Vector2(DEFAULT_DIALOG_WIDTH, DEFAULT_DIALOG_HEIGHT);
        currentDialogueText.rectTransform.sizeDelta = new Vector2(DEFAULT_TEXT_WIDTH, DEFAULT_TEXT_HEIGHT);

        resetOffset();
    }

    void showDialog(Image currentDialogueBox, Text currentDialogueText)
    {
        currentDialogueBox.canvasRenderer.SetAlpha(1);
        currentDialogueText.canvasRenderer.SetAlpha(1);
    }

    IEnumerator runDialogue(List<StringNode> stringNodes, Image currentDialogueBox, Text currentTextBox)
    {
        StringBuilder sb = new StringBuilder();

        int linesShowed = 0;

        while (stringNodes.Count > 0)
        {
            //This adds a line of text to the currentText and then removes the string from the list
            foreach (char c in stringNodes[0].stringNode)
            {
                //AkSoundEngine.PostEvent("AddCharacter", cam.gameObject);
                sb.Append(c);
                currentTextBox.text = sb.ToString();
                if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Joystick1Button1))
                {
                    yield return new WaitForSeconds(fastTypingSpeed);
                }
                else
                {
                    yield return new WaitForSeconds(TypingSpeed);
                }
            }

            linesShowed++;
            sb.AppendLine();
            stringNodes.RemoveAt(0);
            
            if (linesShowed == currentNumberOfLines)
            {
                linesShowed = 0;
                updateCurrentNumberOfLines(stringNodes);
                //If there are strings remaining to show
                if (currentNumberOfLines > 0)
                {
                    //Show info button
                    infoButton.transform.position = new Vector3(currentDialogueBox.transform.position.x + (currentDialogueBox.rectTransform.rect.width / 3),
                                                                currentDialogueBox.transform.position.y - (currentDialogueBox.rectTransform.rect.height / 3), 0);
                    infoButton.canvasRenderer.SetAlpha(1);
                    infoButton.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    //Wait until the player wants to continue reading
                    while (!Input.GetKeyDown(KeyCode.Space) && !Input.GetKeyDown(KeyCode.Joystick1Button1))
                    {

                        yield return null;
                    }
                    infoButton.canvasRenderer.SetAlpha(0);
                    infoButton.transform.localScale = new Vector3(1, 1, 1);

                    currentTextBox.text = "";
                    sb = new StringBuilder();
                    //First we get the width and height target and then we start the transition coroutine
                    int aux_text_width = _stringNodeTools.getTextBoxWidth(stringNodes);
                    int aux_text_height = (int) (font.lineHeight) * currentNumberOfLines;
                    //For the dialog box we need to get the equivalent values using the default ones
                    float aux_dialog_width = currentDialogueBox.rectTransform.rect.width - (currentTextBox.rectTransform.rect.width - aux_text_width);
                    float aux_dialog_height = currentDialogueBox.rectTransform.rect.height - (currentTextBox.rectTransform.rect.height - aux_text_height);
                    //For now, only the dialogue box performs a transition, maybe it's better to make it in both 
                    //while the text is being showed too
                    currentTextBox.rectTransform.sizeDelta = new Vector2(aux_text_width, aux_text_height);

                    inTransition = true;
                    StartCoroutine(smoothTransition(currentDialogueBox.rectTransform, aux_dialog_width, Mathf.Abs(currentDialogueBox.rectTransform.rect.width - aux_dialog_width),
                                                                                      aux_dialog_height, Mathf.Abs(currentDialogueBox.rectTransform.rect.height - aux_dialog_height)));

                    while (inTransition)
                    {
                        yield return null;
                    }
                }
            }
        }
        //Show info button
        infoButton.transform.position = new Vector3(currentDialogueBox.transform.position.x + (currentDialogueBox.rectTransform.rect.width / 3),
                                                            currentDialogueBox.transform.position.y - (currentDialogueBox.rectTransform.rect.height / 3), 0);
        infoButton.canvasRenderer.SetAlpha(1);
        infoButton.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //Wait until the player wants to end dialogue
        while (!Input.GetKeyDown(KeyCode.Space) && !Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            yield return null;
        }
        infoButton.canvasRenderer.SetAlpha(0);
        infoButton.transform.localScale = new Vector3(1, 1, 1);

        ShowingDialogue = false;
        yield return StartCoroutine(outroDialogueAnimation(currentDialogueBox, currentTextBox));
    }

    IEnumerator smoothTransition(RectTransform currentRectTransform, float width_target, float width_target_difference, float height_target, float height_target_difference)
    {
        float width_increment = 0;
        float height_increment = 0;

        //We need to check special conditions
        //------------------------------------------
        //If the width or height difference is greater than 0. In that case, we want to calculate the increment        
        if (width_target_difference > 0)
        {
            width_increment = width_target_difference / TAnimDivision;
            //Also, we need to check if the increment will be positive or negative
            if (currentRectTransform.rect.width > width_target)
            {
                width_increment = -width_increment;
            }
        }
        if (height_target_difference > 0)
        {
            height_increment = height_target_difference / TAnimDivision;

            if (currentRectTransform.rect.height > height_target)
            {
                height_increment = -height_increment;
            }
        }
        //------------------------------------------

        for (int i = 0; i < TAnimDivision; i++)
        {
            currentRectTransform.sizeDelta = new Vector2(currentRectTransform.rect.width + width_increment, currentRectTransform.rect.height + height_increment);
            yield return new WaitForSeconds(TAnimSpeed);
        }

        inTransition = false;
    }

    IEnumerator outroDialogueAnimation(Image currentDialogueBox, Text currentTextBox)
    {
        //Scale stuff        
        Vector3 auxScale = currentTextBox.transform.localScale;
        //Just a decreasing animantion from scale 1 to 0
        while (auxScale.x > 0 || auxScale.y > 0)
        {
            auxScale.x -= 0.05f;
            auxScale.y -= 0.05f;
            currentDialogueBox.transform.localScale = auxScale;
            currentTextBox.transform.localScale = auxScale;

            yield return new WaitForSeconds(IOAnimationSpeed);
        }

        hideDialogue(currentDialogueBox, currentTextBox);
    }

    IEnumerator startDialogueAnimation(List<StringNode> stringNodes, Image currentDialogueBox, Text currentTextBox)
    {
        showDialog(currentDialogueBox, currentTextBox);

        //Scale stuff
        currentDialogueBox.transform.localScale = Vector3.zero;
        Vector3 auxScale = Vector3.zero;
        auxScale.z = 1;
        //Just an incremental animation from 0 to 1
        while (auxScale.x < 1 || auxScale.y < 1)
        {
            auxScale.x += IOIncrement;
            auxScale.y += IOIncrement;
            currentDialogueBox.transform.localScale = auxScale;
            currentTextBox.transform.localScale = auxScale;

            yield return new WaitForSeconds(IOAnimationSpeed);
        }

        currentDialogueBox.transform.localScale = new Vector3(1, 1, 1);
        currentTextBox.transform.localScale = new Vector3(1, 1, 1);
        yield return StartCoroutine(runDialogue(stringNodes, currentDialogueBox,currentTextBox));
    }

    void updateCurrentNumberOfLines(List<StringNode> stringNodes)
    {
        if (stringNodes.Count >= 3)
        {
            currentNumberOfLines = 3;
        }
        else
        {
            currentNumberOfLines = stringNodes.Count;
        }
    }

    void setCurrentDialogueItems(Image currentDialogueBox, Text currentDialogueText)
    {
        auxDialogueBox = currentDialogueBox;
        auxDialogueText = currentDialogueText;
    }

    void resetOffset()
    {
        X_OFFSET = DEFAULT_X_OFFSET;
        Y_OFFSET = DEFAULT_Y_OFFSET;
    }

    IEnumerator waitToEndConversation()
    {
        while (auxDialogueText.canvasRenderer.GetAlpha() > 0 || auxDialogueBox.canvasRenderer.GetAlpha() > 0)
        {
            yield return null;
        }
        IsInteracting = false;
    }

    public void StartInteraction(string dialogue, Image currentDialogueBox, Text currentTextBox)
    {
        setCurrentDialogueItems(currentDialogueBox, currentTextBox);
        IsInteracting = true;
        //Disable player movement
        ShowingDialogue = true;
        List<StringNode> stringNodes = _stringNodeTools.splitText(dialogue, currentTextBox);
        updateCurrentNumberOfLines(stringNodes);
        //In this case, we set the values right after we get them, cause it's the beginning of the conversation and the dialog box is hidden
        //For transitions, we want to keep the values in an auxiliar variable to make a smooth transition
        int aux_text_width = _stringNodeTools.getTextBoxWidth(stringNodes);
        int aux_text_height = font.lineHeight * currentNumberOfLines;
        //For the dialog box we need to get the equivalent values using the default ones
        float aux_dialog_width = currentDialogueBox.rectTransform.rect.width - (currentTextBox.rectTransform.rect.width - aux_text_width);
        float aux_dialog_height = currentDialogueBox.rectTransform.rect.height - (currentTextBox.rectTransform.rect.height - aux_text_height);

        currentTextBox.rectTransform.sizeDelta = new Vector2(aux_text_width, aux_text_height);
        currentDialogueBox.rectTransform.sizeDelta = new Vector2(aux_dialog_width, aux_dialog_height);

        StartCoroutine(startDialogueAnimation(stringNodes, currentDialogueBox, currentTextBox));
    }
    //We call this function from NPCDialogTrigger when the dialogue has ended but we must wait to hide dialogue to interact again
    public void EndOfConvesation()
    {
        StartCoroutine(waitToEndConversation());
    }

    public Dictionary<string, string> GetItemsDictionary()
    {
        return itemsDictionary;
    }

    public Dictionary<string,string> GetDialogDictionary()
    {
        return npcsDictionary;
    }

    public Dictionary<string, string> GetNarratorsDictionary()
    {
        return narratorDictionary;
    }
}
