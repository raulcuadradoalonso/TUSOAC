﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class ElevatorOpenCorridor : MonoBehaviour {

    [Header("References")]
    public PlayableAsset playable;
    public GameObject ElevatorEmitter;
    [Header("Settings")]
    public float X_OFFSET;
    public float Y_OFFSET;

    bool interactionDone = false;
    Image infoButton;

    bool enableInput = false;

    void Start()
    {
        infoButton = GameObject.Find("InfoButton").GetComponent<Image>();
    }

    void Update()
    {
        if (enableInput && !interactionDone)
        {
            infoButton.canvasRenderer.SetAlpha(1);
            infoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                            (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                interactionDone = true;
                infoButton.canvasRenderer.SetAlpha(0);
                GetComponent<PlayableDirector>().Play(playable);
                AkSoundEngine.PostEvent("ElevatorDing", ElevatorEmitter);
                AkSoundEngine.PostEvent("ElevatorOpen", ElevatorEmitter);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = false;
            infoButton.canvasRenderer.SetAlpha(0);
        }        
    }

}
