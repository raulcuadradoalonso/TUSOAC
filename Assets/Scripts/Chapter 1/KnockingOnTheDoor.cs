﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class KnockingOnTheDoor : MonoBehaviour
{
    [Header("References")]
    public ItemDialogTrigger fridgeTrigger;
    public ItemDialogTrigger kitchenWallTrigger;
    public ItemDialogTrigger FloorNote;
    public CubeMovement cm;
    public Image InfoButton;
    public GameObject OST;
    public GameObject KnockEmitter;
    [Header("Settings")]
    public float Y_OFFSET;
    public float X_OFFSET;

    const int LIVINGROOM_TRIGGER_POSITION = 12; //When Cube's x is smaller than this, and he have interacted with at least 1 item, someone knocks

    bool doorTrigger = false;
    bool animationDone = false;
    bool startWaiting = false;

    bool enableInteraction = false;

    float wait = 1.5f; //When the door is knocked, cube movement is disabled during this seconds
    float timer = 0;

	// Use this for initialization
	void Start ()
    {
        FloorNote.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
    {
        //If Cube interacts with the two items in the kitchen OR if he interacts at least with one of them and then he comes back
        //to the living room, someone knocks the door        
        if (((fridgeTrigger.InteractionDone || kitchenWallTrigger.InteractionDone) && cm.transform.position.x < LIVINGROOM_TRIGGER_POSITION)
         &&  !doorTrigger)
        {
            //Knock on the door
            AkSoundEngine.PostEvent("KnockDoor", KnockEmitter);
            //Change music
            AkSoundEngine.PostEvent("SomeoneKnocks", OST);            

            //Enable trigger to open the door
            doorTrigger = true;
            //Enable dialogue trigger with the note in the floor
            FloorNote.gameObject.SetActive(true);

            startWaiting = true;
        }
        if (startWaiting && timer < wait)
        {
            timer += Time.deltaTime;
            cm.enabled = false;
        }
        else if (timer > wait)
        {
            cm.enabled = true;
        }

        if (enableInteraction && doorTrigger && !animationDone)
        {
            InfoButton.canvasRenderer.SetAlpha(1);
            InfoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                            (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                InfoButton.canvasRenderer.SetAlpha(0);
                GetComponent<PlayableDirector>().Play();
                AkSoundEngine.PostEvent("DoorOpen", KnockEmitter);
                animationDone = true;
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInteraction = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        enableInteraction = false;
        InfoButton.canvasRenderer.SetAlpha(0);        
    }
}
