﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class HallMainDoor : MonoBehaviour
{

    public Image InfoButton;
    public float X_OFFSET;
    public float Y_OFFSET;

    public NPCDialogTrigger conciergeConversation;

    public GameObject falseChair;
    public GameObject falseWall;
    public GameObject falseCarpet;
    public GameObject realCarpet;

    bool animationDone = false;
    bool enableInput = false;

    BoxCollider bc;

    void Start()
    {
        bc = GetComponent<BoxCollider>();
        bc.enabled = false;
    }

    void Update()
    {
        if (conciergeConversation.GeneralDialoguesCounter > 0)
        {
            falseChair.SetActive(true);
            falseWall.SetActive(true);
            falseCarpet.SetActive(true);
            realCarpet.SetActive(false);

            bc.enabled = true;
        }

        if (enableInput && !animationDone)
        {
            InfoButton.canvasRenderer.SetAlpha(1);
            InfoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                            (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));

            if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button1)))
            {
                animationDone = true;
                GetComponent<PlayableDirector>().Play();
                InfoButton.canvasRenderer.SetAlpha(0);
                AkSoundEngine.PostEvent("DoorOpen", this.gameObject);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            InfoButton.canvasRenderer.SetAlpha(0);
            enableInput = false;
        }
    }
}
