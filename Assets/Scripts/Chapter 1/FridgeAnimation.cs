﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class FridgeAnimation : MonoBehaviour {

    public ItemDialogTrigger FridgeTrigger;
    public float Time;

	// Use this for initialization
	void Start ()
    {
        FridgeTrigger.afterInteractionFunction = openCloseFridge;
	}

    void openCloseFridge()
    {
        GetComponent<PlayableDirector>().Play();
        StartCoroutine(fridgeSounds());
    }

    IEnumerator fridgeSounds()
    {
        AkSoundEngine.PostEvent("OpenFridge", this.gameObject);
        yield return new WaitForSeconds(Time);
        AkSoundEngine.PostEvent("CloseFridge", this.gameObject);
    }
}
