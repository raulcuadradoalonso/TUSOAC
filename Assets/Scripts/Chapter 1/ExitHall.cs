﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExitHall : MonoBehaviour
{

    public Image fader;

    void Start()
    {
        fader.gameObject.SetActive(true);
        fader.canvasRenderer.SetAlpha(0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("doing it");
            other.gameObject.GetComponent<PlayerDialog>().IsInteracting = true;
            other.gameObject.GetComponent<CubeMovement>().enabled = false;
            StartCoroutine(loadScene());
        }
    }

    IEnumerator loadScene()
    {
        fader.CrossFadeAlpha(1, 1.5f, true);
        AkSoundEngine.PostEvent("StopEverything", this.gameObject);

        yield return new WaitForSeconds(1.5f);

        SceneManager.LoadScene(2);
    }
}
