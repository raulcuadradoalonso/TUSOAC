﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ElevatorMovement : MonoBehaviour {

    public GameObject ElevatorExternalEmitterCorridor;
    public GameObject ElevatorExternalEmitterHall;
    public GameObject ElevatorInsideEmitter;
    public GameObject OSTEmitter;
    public PlayableAsset playableCorridor;
    public PlayableAsset playableHall;
    //To change the camera point of view
    public Transform cameraTargetElevatorCorridor;
    public Transform cameraTargetElevatorHall;
    public Camera cam;
    //To make the camera children of this so it moves with the elevator
    public Transform elevator;
    //Time that takes the camera changes of point of view
    public float FirstTransitionTime = 2;
    public float SecondTransitionTime = 0.5f;
    //To change elevator buttons color during the movement
    public GameObject[] buttons;
    public Color Green;
    public Color EmisionGreen;
    public Color Red;
    public Color EmissionRed;
    public Color Yellow;
    public Color EmissionYellow;
    public float elevatorMovementDuration;
    //public Transform player;

    bool animationDone = false;
    bool cameraMoved = false;
    bool resetRotation = false;
    bool setparent = false;

    Vector3 velocity = new Vector3 (1, 1, 1);
    float firstTimer = 0; //For the camera movement in the corridor

	void Update ()
    {
        if (animationDone)
        {
            firstTimer += Time.deltaTime;
            if (firstTimer >= FirstTransitionTime + 0.1f) //We add 0.1f for accuracy issues
            {
                cameraMoved = true;
            }
        }
        //We move the camera setting the elevator as his parent so the camera moves smoothly
        if (cameraMoved)
        {
            if (!setparent) { cam.transform.SetParent(elevator); setparent = true; }
            //When the timeline ends, we give the control back to the player perfoming a smooth transition of the camera
            if (GetComponent<PlayableDirector>().state != PlayState.Playing && !resetRotation)
            {
                cameraMoved = false;
                resetRotation = true;
                cam.transform.parent = null;
                GetComponent<PlayableDirector>().Play(playableHall);
                AkSoundEngine.PostEvent("ElevatorOpen", ElevatorExternalEmitterHall);
                AkSoundEngine.PostEvent("StopMusic", OSTEmitter);
                //cam.GetComponent<CameraTween>().UpdateCameraView(cameraTargetElevatorHall.position, cameraTargetElevatorHall.rotation.eulerAngles.x, SecondTransitionTime);
            }
        }        
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !animationDone)
        {
            //When the player enters into the elevator we disable the movement and start a timeline animation
            animationDone = true;
            cam.GetComponent<CameraMovement>().Stop();
            cam.GetComponent<CameraTween>().MoveCameraToStaticPosition(cameraTargetElevatorCorridor.position, cameraTargetElevatorCorridor.rotation.eulerAngles.x, FirstTransitionTime, false, cameraTargetElevatorCorridor.GetComponent<CameraTrigger>());
            GetComponent<PlayableDirector>().Play(playableCorridor);
            //Change the color of the buttons
            StartCoroutine(switchButtonColors());            
        }
    }

    IEnumerator switchButtonColors()
    {
        yield return new WaitForSeconds(0.5f);
        AkSoundEngine.PostEvent("ElevatorClose", ElevatorExternalEmitterCorridor);
        yield return new WaitForSeconds(3f);
        AkSoundEngine.PostEvent("ElevatorMoves", ElevatorInsideEmitter);
        yield return new WaitForSeconds(0.5f);
        buttons[buttons.Length - 1].GetComponent<MeshRenderer>().materials[1].color = Yellow;
        buttons[buttons.Length - 1].GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", EmissionYellow);
        float timeToSwitch = elevatorMovementDuration / buttons.Length;
        for (int i = 0; i < buttons.Length; i++)
        {
            if (i < buttons.Length - 1)
            {
                buttons[i].GetComponent<MeshRenderer>().materials[1] = new Material(buttons[i].GetComponent<MeshRenderer>().material);
                buttons[i].GetComponent<MeshRenderer>().materials[1].color = Yellow;
                buttons[i].GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", EmissionYellow);
            }
            else
            {
                buttons[i].GetComponent<MeshRenderer>().materials[1] = new Material(buttons[i].GetComponent<MeshRenderer>().material);
                buttons[i].GetComponent<MeshRenderer>().materials[1].color = Green;
                buttons[i].GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", EmisionGreen);                
            }

            if (i != 0)
            {
                buttons[i - 1].GetComponent<MeshRenderer>().materials[1] = new Material(buttons[i].GetComponent<MeshRenderer>().material);
                buttons[i - 1].GetComponent<MeshRenderer>().materials[1].color = Red;
                buttons[i - 1].GetComponent<MeshRenderer>().materials[1].SetColor("_EmissionColor", EmissionRed);
            }

            yield return new WaitForSeconds(timeToSwitch);
        }
        AkSoundEngine.PostEvent("StopElevator", ElevatorInsideEmitter);        
        AkSoundEngine.PostEvent("ElevatorDing", ElevatorExternalEmitterHall);

        cam.GetComponent<CameraTween>().FromStaticToResumeMovement(cameraTargetElevatorHall.position, cameraTargetElevatorHall.rotation.eulerAngles.x, SecondTransitionTime, 0.5f, cameraTargetElevatorHall.GetComponent<CameraTrigger>());
        
    }
}
