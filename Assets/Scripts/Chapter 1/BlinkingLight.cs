﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkingLight : MonoBehaviour {

    public Material TVScreen;

    Light lght;    

    float timer;
    float threshold = 1f;
    float intensityChanges = 0.6f;
    float colorChanges = 0.2f;

    float lastValue;

	// Use this for initialization
	void Start ()
    {
        lght = GetComponent<Light>();
        Color eColor = Color.black;
        eColor.r = 0.9f;
        eColor.g = 0.9f;
        eColor.b = 0.9f;
        TVScreen.SetColor("_EmissionColor", eColor);
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        timer += Time.deltaTime;

		if (timer > threshold)
        {   
            lght.intensity += intensityChanges;
            changeEmissionColor();

            if (intensityChanges > 0)
            {
                intensityChanges = - Random.Range(0.1f, 0.35f);
                colorChanges = -Random.Range(0.1f, 0.2f);
            }
            else
            {
                intensityChanges = Random.Range(0.1f, 0.35f);
                colorChanges = Random.Range(0.1f, 0.2f);
            }

            threshold = Random.Range(0.8f, 3f);
            timer = 0;
        }
        
	}

    void changeEmissionColor()
    {
        Color eColor = TVScreen.GetColor("_EmissionColor");

        eColor.r += colorChanges;
        eColor.g += colorChanges;
        eColor.b += colorChanges;

        if (eColor.r > 1f)
        {
            eColor.r = 1;
            eColor.g = 1;
            eColor.b = 1;
        }
        else if (eColor.r < 0.8f)
        {
            eColor.r = 0.8f;
            eColor.g = 0.8f;
            eColor.b = 0.8f;
        }

        TVScreen.SetColor("_EmissionColor", eColor);
    }
}
