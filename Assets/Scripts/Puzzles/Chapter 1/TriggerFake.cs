﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class TriggerFake : MonoBehaviour {

    public PlayableDirector m_PlayableDirector;
    public Image fader;
    public GameObject narratorTrigger;

    void Start()
    {
        fader.canvasRenderer.SetAlpha(1);
        fader.gameObject.SetActive(true);
        fader.CrossFadeAlpha(0, 1, true);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<CubeMovement>().enabled = false;
            other.GetComponent<Rigidbody>().drag = 0;
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
            m_PlayableDirector.Play();
            AkSoundEngine.PostEvent("OpenGates", this.gameObject);
            StartCoroutine(waitToEnd());
        }
    }

    IEnumerator waitToEnd()
    {
        if (narratorTrigger.activeSelf)
        {
            yield return new WaitForSeconds(3f);
        }

        fader.canvasRenderer.SetAlpha(0);
        fader.gameObject.SetActive(true);
        fader.CrossFadeAlpha(1, 1.5f, true);

        while (fader.canvasRenderer.GetAlpha() < 1)
        {
            yield return null;
        }
        AkSoundEngine.PostEvent("StopEverything", this.gameObject);
        SceneManager.LoadScene(3);
    }
}
