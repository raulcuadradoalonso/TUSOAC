﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ButtonBridge : MonoBehaviour {

    public PlayableDirector m_PlayableDirector;
    public GameObject narratorTrigger;

    public float timeToWait1 = 1f;
    public float timeToWait2 = 0.7f;
    public float timeToWait3 = 1f;

    ActionButton ab;
    PlayerDialog pd;

    string key1 = "chapter1_p_bridge";
    string key2 = "chapter1_p_bridge_2";

    void Start ()
    {
        narratorTrigger.SetActive(false);

        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);

        pd = GameObject.Find("CubePlayer").GetComponent<PlayerDialog>();
	}

    void doSomething()
    {
        narratorTrigger.SetActive(true);
        ab.IsDoingSomething = true;
        StartCoroutine(interacting());
    }

    IEnumerator interacting()
    {
        pd.X_OFFSET = pd.X_OFFSET * 2;
        pd.Y_OFFSET = pd.Y_OFFSET * 2;

        if (pd.GetItemsDictionary().ContainsKey(key1))
        {
            pd.StartInteraction(pd.GetItemsDictionary()[key1], pd.DialogueBox, pd.DialogueText);
        }

        while(pd.ShowingDialogue)
        {
            yield return null;
        }

        yield return new WaitForSeconds(timeToWait1);

        ab.PressButton();
        ab.enabled = false;

        yield return new WaitForSeconds(timeToWait2);

        m_PlayableDirector.Play();
        AkSoundEngine.PostEvent("OpenGates", this.gameObject);

        while(m_PlayableDirector.state == PlayState.Playing)
        {
            yield return null;
        }

        yield return new WaitForSeconds(timeToWait3);

        pd.X_OFFSET = pd.X_OFFSET * 2;
        pd.Y_OFFSET = pd.Y_OFFSET * 2;

        if (pd.GetItemsDictionary().ContainsKey(key2))
        {
            pd.StartInteraction(pd.GetItemsDictionary()[key2], pd.DialogueBox, pd.DialogueText);
        }

        while (pd.ShowingDialogue)
        {
            yield return null;
        }

        pd.EndOfConvesation();
    }
}
