﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonStopsCamera : MonoBehaviour {
    
    public Camera Cam;
    public CubeMovement cm;
    public Transform DefaultCameraPos;
    public BoxCollider LeftDefaultPosTrigger;
    public BoxCollider RightDefaultPosTrigger;
    public Transform LeftTeleport;
    public Transform RightTeleport;
    public float TransitionTime;

    int X_LEFT_VALUE_TRIGGER = 116;
    bool teleportationActivated = false;
    bool readyToTeleport = true;

    ActionButton ab;

    void Start()
    {
        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);
    }

    void Update ()
    {
        //Para una mejor función de transport igual lo mejor sería duplicar el cubo
        //La distancia a la que están, calcularla para una anchura de pantalla (por ejemplo 1080)
        //Y si hubiera otra anchura, hallar la distancia entre cubos por regla de tres
        if (!cm.IsVisible && teleportationActivated && readyToTeleport)
        {
            readyToTeleport = false;
            if (cm.transform.position.x < Cam.transform.position.x)
            {
                cm.transform.position = new Vector3(RightTeleport.position.x, cm.transform.position.y, cm.transform.position.z);
            }
            else if (cm.transform.position.x > Cam.transform.position.x)
            {
                cm.transform.position = new Vector3(LeftTeleport.position.x, cm.transform.position.y, cm.transform.position.z);
            }
        }

        if (cm.IsVisible)
        {
            readyToTeleport = true;
        }
	}

    public void doSomething()
    {
        teleportationActivated = !teleportationActivated;

        Debug.Log("Button pressed, teleportation is now: " + teleportationActivated);

        if (teleportationActivated)
        {
            ab.PressButton();
            LeftDefaultPosTrigger.enabled = false;
            RightDefaultPosTrigger.enabled = false;
        }
        else
        {
            ab.ReleaseButton();
            LeftDefaultPosTrigger.enabled = true;
            RightDefaultPosTrigger.enabled = true;
        }

        //ab.IsDoingSomething = false;
    }
}
