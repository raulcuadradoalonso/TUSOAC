﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using DigitalRuby.Tween;

public class ButtonDoesNothing : MonoBehaviour {

    public PlayableDirector m_PlayableDirector;
    public Transform refInitialPos;
    public float TransitionTime;

    public float TimeToWait1 = 2f;

    ActionButton ab;
    PlayerDialog pd;

    string key = "chapter1_p_lastpuzzle";

    bool positioned = false;

	void Start ()
    {
        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);

        pd = GameObject.Find("CubePlayer").GetComponent<PlayerDialog>();
	}

    void doSomething()
    {
        ab.IsDoingSomething = true;
        StartCoroutine(interacting());
    }

    IEnumerator interacting()
    {

        ab.PressButton(true);

        pd.GetComponent<CubeMovement>().enabled = false;

        yield return new WaitForSeconds(TimeToWait1);

        pd.gameObject.Tween("Moving cube", pd.transform.position, refInitialPos.position, TransitionTime, TweenScaleFunctions.QuadraticEaseInOut,
            (t) =>
            {
                pd.transform.position = t.CurrentValue;
            }, (t) =>
            {

            });

        pd.gameObject.Tween("Rotating cube", pd.transform.rotation, refInitialPos.rotation, TransitionTime, TweenScaleFunctions.QuadraticEaseInOut,
            (t) =>
            {
                pd.transform.rotation = t.CurrentValue;
            }, (t) =>
            {
                positioned = true;
            });        

        if (pd.GetItemsDictionary().ContainsKey(key))
        {
            pd.StartInteraction(pd.GetItemsDictionary()[key], pd.DialogueBox, pd.DialogueText);
        }

        while (pd.ShowingDialogue || !positioned)
        {
            yield return null;
        }

        m_PlayableDirector.Play();
        pd.EndOfConvesation();
        pd.GetComponent<CubeMovement>().enabled = true;
    }
}
