﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonResumesCamera : MonoBehaviour
{
    public Camera Cam;
    public ButtonStopsCamera bsc;
    public Transform Cube;
    public float TransitionTime;
    public GameObject narratorTrigger;
    public BoxCollider rightTrigger;

    ActionButton ab;

    void Start ()
    {
        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);
        narratorTrigger.SetActive(false);
	}


    public void doSomething()
    {
        narratorTrigger.SetActive(true);
        rightTrigger.enabled = true;

        ab.PressButton(true);     

        Destroy(bsc);
        Destroy(this);

        //ab.IsDoingSomething = false;
    }
}
