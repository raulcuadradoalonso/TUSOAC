﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class ButtonMovesWall : MonoBehaviour {

    public Transform Wall;
    public Transform TargetPosition;
    public float TransitionTime;
    public string PressButtonSound;
    public string ReleaseButtonSound;

    Vector3 defaultPos;
    bool open = false;

    ActionButton ab;

	void Start ()
    {
        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);

        defaultPos = new Vector3 (Wall.transform.position.x, Wall.transform.position.y, Wall.transform.position.z);
	}       

    public void doSomething()
    {
        
        Vector3 initialPos = Wall.transform.position;
        Vector3 endPos = TargetPosition.position;

        //Close
        if (open)
        {
            open = false;
            ab.ReleaseButton(ReleaseButtonSound);
            endPos = defaultPos;
        }   
		//Open		
        else
        {
            open = true;
            ab.PressButton(false, PressButtonSound);
        }
		//Wall animation using the Tween class
        gameObject.Tween("Moving wall", initialPos, endPos, TransitionTime, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            Wall.transform.position = t.CurrentValue;
        }, (t) =>
        {
            //Completion
        });
    }
}
