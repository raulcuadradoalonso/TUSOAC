﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonDisablesCollisions : MonoBehaviour
{
    public GameObject[] Walls;

    public bool collidersEnabled = true;

    ActionButton ab;

	void Start ()
    {
        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);
    }
	
	void Update ()
    {
		
	}

    public void doSomething()
    {

        collidersEnabled = !collidersEnabled;

        if (collidersEnabled)
        {
            ab.ReleaseButton();
        }
        else
        {
            ab.PressButton();
        }

        for (int i = 0; i < Walls.Length; i++)
        {
            Walls[i].GetComponent<BoxCollider>().enabled = collidersEnabled;
        }

        //ab.IsDoingSomething = false;
    }
}
