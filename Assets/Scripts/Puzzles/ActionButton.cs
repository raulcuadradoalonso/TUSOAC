﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class ActionButton : MonoBehaviour {

    [Header("References")]
    public Image infoButton;
    public GameObject m_Button;
    public Transform PressedPosRef;

    [Header("Settings")]
    public float X_OFFSET;
    public float Y_OFFSET;    
    public float ButtonPressTime;

    public Color GreenColor;
    public Color GreenEmissionColor;
    public Color RedColor;
    public Color RedEmisionColor;

    [HideInInspector] public bool IsDoingSomething = false;

    Vector3 releasedButtonPosRef;
    bool posRefInitialized = false;

    Action doSomethingFunc;

    bool enableInput = false;

    void Start()
    {
        infoButton = GameObject.Find("InfoButton").GetComponent<Image>();
        infoButton.canvasRenderer.SetAlpha(0);
    }

    void Update()
    {
        if (enableInput && !IsDoingSomething)
        {
            showInfoButton();

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                IsDoingSomething = true;
                doSomething();
            }
            
        }
        else if (enableInput && IsDoingSomething)
        {
            hideInfoButton();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = true;
        }        
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = false;
            hideInfoButton();
        }
    }

    public void showInfoButton()
    {
        infoButton.canvasRenderer.SetAlpha(1);
        infoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                        (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
    }

    public void hideInfoButton()
    {
        infoButton.canvasRenderer.SetAlpha(0);
    }
    

    public void PressButton(bool disableButton = false, string pressButtonSound = "PressButton")
    {

        if (pressButtonSound == "") { pressButtonSound = "PressButton"; }

        AkSoundEngine.PostEvent(pressButtonSound, this.gameObject);

        if (!posRefInitialized)
        {
            posRefInitialized = true;
            releasedButtonPosRef = m_Button.transform.localPosition;
        }

        m_Button.Tween("Press button", m_Button.transform.localPosition, PressedPosRef.localPosition, ButtonPressTime,
            TweenScaleFunctions.QuadraticEaseInOut, (t) =>
            {
                //Progression
                m_Button.transform.localPosition = t.CurrentValue;
            }, (t) =>
            {
                //Completion
                m_Button.GetComponent<MeshRenderer>().material = new Material(m_Button.GetComponent<MeshRenderer>().material);
                m_Button.GetComponent<MeshRenderer>().material.color = GreenColor;
                m_Button.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", GreenEmissionColor);

                if (!disableButton) { IsDoingSomething = false; }                
            });
    }

    public void ReleaseButton(string releaseButtonSound = "ReleaseButton")
    {
        if (releaseButtonSound == "") { releaseButtonSound = "ReleaseButton"; }

        AkSoundEngine.PostEvent(releaseButtonSound, this.gameObject);

        m_Button.Tween("Press button", m_Button.transform.localPosition, releasedButtonPosRef, ButtonPressTime,
            TweenScaleFunctions.QuadraticEaseInOut, (t) =>
            {
                //Progression
                m_Button.transform.localPosition = t.CurrentValue;
            }, (t) =>
            {
                //Completion
                m_Button.GetComponent<MeshRenderer>().material = new Material(m_Button.GetComponent<MeshRenderer>().material);
                m_Button.GetComponent<MeshRenderer>().material.color = RedColor;
                m_Button.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", RedEmisionColor);

                IsDoingSomething = false;
            });
    }

    public void doSomething()
    {
        doSomethingFunc();
    }

    public void SetActionFunc(Action callback)
    {
        doSomethingFunc = callback;
    }
}
