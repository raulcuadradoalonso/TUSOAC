﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;
using UnityEngine.Playables;

public class TimelineToCity : MonoBehaviour {
    [Header("References")]
    public BoxCollider invisibleWall;
    public CameraTween cam;
    public CameraTrigger ct;
    public CarRandomSpawm CRS;
    public PlayableDirector creditsPD;
    [Header("Settings")]
    public Vector3 FirstTargetPosition;
    public Vector3 SecondTargetPosition;
    public float TransitionTime;
    public float SecondTransitionTime;
    public float RotationTime;
    public float CamTransTime;
    [Header("Car settings")]
    public GameObject car;
    public float carEndPos;
    public float carTime;

    private void Start()
    {
        CRS.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //MOVING CUBE BEFORE STARTING THE TIMELINE
            other.gameObject.GetComponent<CubeMovement>().enabled = false;
            other.gameObject.Tween("Moving cube", other.gameObject.transform.position, FirstTargetPosition, TransitionTime, TweenScaleFunctions.Linear,
                (t) =>
                {
                    //Progress
                    other.gameObject.transform.position = t.CurrentValue;

                }, (t) =>
                {
                    //Completion
                    cam.MoveCameraToStaticPosition(ct.transform.position, ct.transform.rotation.eulerAngles.x, CamTransTime, false, ct);
                    moveCar();
                    other.gameObject.Tween("Moving cube 2", other.gameObject.transform.position, SecondTargetPosition, SecondTransitionTime, TweenScaleFunctions.QuadraticEaseInOut,
                        (x) =>
                        {
                            //Progress
                            other.gameObject.transform.position = x.CurrentValue;

                        }, (x) =>
                        {
                            //Completion
                            cam.GetComponent<CameraMovement>().ResumeEverything();
                            cam.cameraStatic = false;
                            other.gameObject.GetComponent<CubeMovement>().enabled = true;
                            invisibleWall.enabled = true;
                            creditsPD.Play();
                        });
                });
            
            //ROTATING CUBE
            other.gameObject.Tween("Rotate Cube", other.transform.rotation, Quaternion.Euler(0, 90, 0), RotationTime, TweenScaleFunctions.SineEaseInOut, (t) =>
            {
                //Progress
                other.transform.rotation = t.CurrentValue;
            }, (t) =>
            {
                //Completion
            });
        }
    }

    void moveCar()
    {
        AkSoundEngine.PostEvent("CarMoving", car);

        car.Tween("Car moving", car.transform.position.x, carEndPos, carTime, TweenScaleFunctions.Linear,
            (t) =>
            {
                //Progress
                car.transform.position = new Vector3(t.CurrentValue, car.transform.position.y, car.transform.position.z);
            }, (t) =>
            {
                //Completion
            });

        CRS.enabled = true;
    }
}
