﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEnablesInvisibleBridge : MonoBehaviour
{
    public BoxCollider bridge;
    public BoxCollider wall;

    ActionButton ab;

    bool collidersEnabled;

	void Start ()
    {
        ab = GetComponent<ActionButton>();
        ab.SetActionFunc(doSomething);
    }

    public void doSomething()
    {

        collidersEnabled = !collidersEnabled;
        bridge.enabled = collidersEnabled;
        wall.enabled = !collidersEnabled;

        if (!collidersEnabled)
        {
            ab.ReleaseButton();
        }
        else
        {
            ab.PressButton();
        }        
    }
}
