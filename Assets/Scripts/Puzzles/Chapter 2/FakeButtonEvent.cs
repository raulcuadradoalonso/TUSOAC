﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FakeButtonEvent : MonoBehaviour
{
    [Header("Objects references")]
    public GameObject Button;
    public GameObject BaseButton;
    public GameObject FakeButton;
    public GameObject FakeBaseButton;
    public BoxCollider TriggerBoxCollider;

    [Header("Localization references")]
    public string Narrator1;
    public string Cube1;
    public string Narrator2;

    [Header("Timings")]
    public float TimeBeforeFirstInteraction;
    public float TimeBeforeHidingNarratorsText;

    PlayerDialog pd;
    Text narratorsText;

    void Start()
    {
        Button.SetActive(false);
        BaseButton.SetActive(false);
        TriggerBoxCollider.enabled = false;
        FakeBaseButton.SetActive(true);
        FakeButton.SetActive(true);

        pd = GameObject.Find("CubePlayer").GetComponent<PlayerDialog>();
        narratorsText = GameObject.Find("NarratorsText").GetComponent<Text>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(fakeInteraction());
            GetComponent<BoxCollider>().enabled = false;
        }
    }

    IEnumerator fakeInteraction ()
    {
        pd.IsInteracting = true;

        yield return new WaitForSeconds(TimeBeforeFirstInteraction);

        //Interaction narrator
        if (pd.GetNarratorsDictionary().ContainsKey(Narrator1))
        {
            narratorsText.color = new Color(narratorsText.color.r, narratorsText.color.g, narratorsText.color.b, 1);
            narratorsText.canvasRenderer.SetAlpha(0);
            narratorsText.CrossFadeAlpha(1, 1, true);
            narratorsText.text = pd.GetNarratorsDictionary()[Narrator1];
        }

        yield return new WaitForSeconds(TimeBeforeHidingNarratorsText);
        narratorsText.CrossFadeAlpha(0, 1, true);

        //Interaction cube
        if (pd.GetNarratorsDictionary().ContainsKey(Cube1))
        {
            pd.StartInteraction(pd.GetNarratorsDictionary()[Cube1], pd.DialogueBox, pd.DialogueText);
        }

        while (pd.ShowingDialogue)
        {
            yield return null;
        }

        //Interaction narrator
        if (pd.GetNarratorsDictionary().ContainsKey(Narrator2))
        {
            narratorsText.CrossFadeAlpha(1, 1, true);
            narratorsText.text = pd.GetNarratorsDictionary()[Narrator2];
        }

        yield return new WaitForSeconds(TimeBeforeHidingNarratorsText * 0.5f);

        //Play typing sound effect
        AkSoundEngine.PostEvent("TypeKeyboard", this.gameObject);

        yield return new WaitForSeconds(TimeBeforeHidingNarratorsText * 0.5f);
        narratorsText.CrossFadeAlpha(0, 1, true);        

        //Wait until the narrators text is hidden
        while (narratorsText.canvasRenderer.GetAlpha() > 0)
        {
            yield return null;
        }

        //Play sound effect of modifying the position of the button
        AkSoundEngine.PostEvent("PressButton", this.gameObject);

        //Reset narrators canvas
        narratorsText.text = "";
        narratorsText.CrossFadeAlpha(1, 0, true);
        narratorsText.color = new Color(narratorsText.color.r, narratorsText.color.g, narratorsText.color.b, 0);

        //Enable buttons
        Button.SetActive(true);
        BaseButton.SetActive(true);
        TriggerBoxCollider.enabled = true;
        FakeBaseButton.SetActive(false);
        FakeButton.SetActive(false);

        pd.IsInteracting = false;
    }
}
