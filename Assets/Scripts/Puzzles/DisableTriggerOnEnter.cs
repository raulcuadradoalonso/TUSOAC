﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTriggerOnEnter : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}
