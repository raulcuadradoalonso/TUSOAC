﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour {

    [Header("Stats")]
    //public float MaxSpeed;
    public float Speed;
    public float rotationSpeed;
    public float InputThreshold;

    public bool IsVisible;

    Rigidbody rb;
    CameraMovement cam;
    PlayerDialog pd;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = GameObject.Find("Main Camera").GetComponent<CameraMovement>();
        pd = GetComponent<PlayerDialog>();
    }

    void FixedUpdate()
    {
        if (!pd.IsInteracting)
        {
            //Input movement
            float moveHorizontal = Input.GetAxis("Horizontal");
            Vector3 movement = new Vector3(moveHorizontal, 0, 0);

            if (Mathf.Abs(moveHorizontal) > InputThreshold)
            {
                //Basic movement
                movement = new Vector3(moveHorizontal, 0, 0);
                rb.AddForce(movement * Speed);

                //Rotation
                if (movement != Vector3.zero)
                {
                    Quaternion lookRotation = Quaternion.LookRotation(movement);
                    transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);
                }
            }            
        }
    }

    void OnTriggerEnter(Collider other)
    {        
        cam.UpdateCameraView(other);
    }

    void OnBecameInvisible()
    {
        IsVisible = false;
    }

    void OnBecameVisible()
    {
        IsVisible = true;
    }
}
