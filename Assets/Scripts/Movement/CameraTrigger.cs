﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraTrigger : MonoBehaviour
{
    [Header("Movement settings")]
    public bool moveToStaticPosition;
    public bool stopCube;
    [Header("Lens settings")]
    public float FocusDistance;
    public float Aperture;
    public float FocalLength;

    public void GetLensState()
    {
        //Storing post-processing current state
        PostProcessingProfile ppp = Camera.main.GetComponent<PostProcessingBehaviour>().profile;
        FocusDistance = ppp.depthOfField.settings.focusDistance;
        Aperture = ppp.depthOfField.settings.aperture;
        FocalLength = ppp.depthOfField.settings.focalLength;
    }
}
