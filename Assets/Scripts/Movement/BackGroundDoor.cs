﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class BackGroundDoor : MonoBehaviour {

    [Header("References")]
    public GameObject TargetDoor;
    public CameraMovement cam;
    [Header("Settings")]
    public float X_OFFSET;
    public float Y_OFFSET;
    public float TimeToEnter;
    public float TimeToRotate;
    public float TimeToExit;
    public float TimeCamTransition;
    public bool ShouldCameraMove;

    Transform EnterPosRef;
    Transform DoorTargetCameraPosition;
    Transform TeleportCubePosRef;
    Transform ExitPosRef;

    Image infoButton;
    GameObject cube;

    bool enableInput = false;
    bool isEnteringDoor = false;

    void Start()
    {
        infoButton = GameObject.Find("InfoButton").GetComponent<Image>();

        //Set up of the door
        EnterPosRef = transform.Find("EnterPosRef");
        ExitPosRef = TargetDoor.transform.Find("ExitPosRef");
        TeleportCubePosRef = TargetDoor.transform.Find("EnterPosRef");
        if (ShouldCameraMove)
        {
            //Reference door target camera pos
        }
    }

    void Update()
    {
        if (enableInput && !isEnteringDoor)
        {
            showInfoButton();

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                isEnteringDoor = true;
                StartCoroutine(doorAnimation());
            }

        }
        else if (enableInput && isEnteringDoor)
        {
            hideInfoButton();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = true;
            cube = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            enableInput = false;
            hideInfoButton();
        }
    }

    public void showInfoButton()
    {
        infoButton.canvasRenderer.SetAlpha(1);
        infoButton.transform.position = Camera.main.WorldToScreenPoint(new Vector3
                                        (transform.position.x + X_OFFSET, transform.position.y + Y_OFFSET, transform.position.z));
    }

    public void hideInfoButton()
    {
        infoButton.canvasRenderer.SetAlpha(0);
    }

    IEnumerator doorAnimation()
    {
        cube.GetComponent<CubeMovement>().enabled = false;

        //ENTERING THE DOOR

        if (ShouldCameraMove) { cam.StopEverything(); }

        cube.gameObject.Tween("Moving Cube", cube.transform.position, EnterPosRef.position, TimeToEnter, TweenScaleFunctions.SineEaseIn, 
            (t) =>
            {
                //Progress
                cube.transform.position = t.CurrentValue;
            }, (t) =>
            {
                //Completion
            });

        cube.gameObject.Tween("Rotate Cube", cube.transform.rotation, EnterPosRef.transform.rotation, TimeToRotate, TweenScaleFunctions.SineEaseInOut, (t) =>
        {
            //Progress
            cube.transform.rotation = t.CurrentValue;
        }, (t) =>
        {

        });

        yield return new WaitForSeconds(TimeToEnter);

        //TELEPORTING CUBE TO THE NEW POSITION AND UPDATING CAMERA POS

        if (ShouldCameraMove)
        {
            //Updating camera position
        }

        //Cube is being teleported and we can enable the trigger of the door again
        isEnteringDoor = false;
        cube.transform.position = TeleportCubePosRef.position;
        cube.transform.rotation = Quaternion.Euler(0, 180, 0);

        //LEAVING FROM THE DOOR

        cube.gameObject.Tween("Moving Cube", cube.transform.position, ExitPosRef.position, TimeToExit, TweenScaleFunctions.SineEaseOut,
            (t) =>
            {
                //Progress
                cube.transform.position = t.CurrentValue;
            }, (t) =>
            {
                //Completion
                cube.GetComponent<CubeMovement>().enabled = true;                
            });
    }
}
