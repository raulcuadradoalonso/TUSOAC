﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using DigitalRuby.Tween;

public class CameraTween : MonoBehaviour
{
    public bool cameraStatic = false;

    float default_focusDistance;
    float default_aperture;
    float default_focalLength;

    CameraMovement cameraMovement;
    CubeMovement cubeMovement;

    private void Awake()
    {
        PostProcessingProfile aux_ppp = GetComponent<PostProcessingBehaviour>().profile;
        default_focusDistance = aux_ppp.depthOfField.settings.focusDistance;
        default_focalLength = aux_ppp.depthOfField.settings.focalLength;
        default_aperture = aux_ppp.depthOfField.settings.aperture;
    }

    void Start ()
    {
        cameraMovement = GetComponent<CameraMovement>();
        cubeMovement = GameObject.Find("CubePlayer").GetComponent<CubeMovement>();
	}

    public void MoveCameraToStaticPosition(Vector3 targetPosition, float targetAngle, float time, bool stopCube = false, CameraTrigger ct = null)
    {

        cameraStatic = true;

        cameraMovement.StopEverything();

        if (stopCube) { cubeMovement.enabled = false; }

        Vector3 initialPos = transform.position;
        Vector3 endPos = targetPosition;
        Vector3 currentPos = transform.position;

        //UPDATING POSITION

        gameObject.Tween("Updating camera view", initialPos, endPos, time, TweenScaleFunctions.QuadraticEaseInOut, (t) =>
        {
            //Progress
            transform.position = t.CurrentValue;
        }, (t) =>
        {
            //Completion            
        });

        //UPDATING ANGLES
        float startAngle = transform.rotation.eulerAngles.x;
        rotateCamera(startAngle, targetAngle, time, stopCube);

        if (ct != null)
        {
            //UPDATING LENS SETTINGS

            updateLensSettings(ct.FocusDistance, ct.Aperture, ct.FocalLength, time);
        }
    }

    public void UpdateCameraView(Vector3 targetPosition, float targetAngle, float time, CameraTrigger ct = null)
    {
        cameraStatic = false;

        //Initial, current and end position
        Vector3 initialPos = transform.position;
        Vector3 endPos = targetPosition;
        endPos.x = cubeMovement.transform.position.x;

        //UPDATING POSITION

        gameObject.Tween("Updating camera view", initialPos, endPos, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            transform.position = new Vector3(transform.position.x, t.CurrentValue.y, t.CurrentValue.z);
        }, (t) =>
        {
            //Completion
            cameraMovement.Resume();
        });

        //UPDATING ANGLES

        float startAngle = transform.rotation.eulerAngles.x;
        rotateCamera(startAngle, targetAngle, time, false); //The false at the end is to indicate if we want to stop Cube (in this function we never want)

        if (ct != null)
        {
            //UPDATING LENS SETTINGS

            updateLensSettings(ct.FocusDistance, ct.Aperture, ct.FocalLength, time);
        }        
    }

    public void FromStaticToResumeMovement(Vector3 targetPosition, float targetAngle, float time, float initialSmoothTime, CameraTrigger ct = null)
    {
        cameraStatic = false;
        //Updating camera movement settings
        cameraMovement.StopEverything();
        cameraMovement.smoothTime = initialSmoothTime;
        //Initial, current and end position
        Vector3 initialPos = transform.position;
        Vector3 endPos = targetPosition;
        endPos.x = cubeMovement.transform.position.x;

        //UPDATING POSITION

        gameObject.Tween("Updating camera view", initialPos, endPos, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            cameraMovement.ResumeEverything();
            transform.position = new Vector3(transform.position.x, t.CurrentValue.y, t.CurrentValue.z);
        }, (t) =>
        {
            //Completion
            cameraMovement.Resume();
        });

        //UPDATING ANGLES

        float startAngle = transform.rotation.eulerAngles.x;
        rotateCamera(startAngle, targetAngle, time, false); //The false at the end is to indicate if we want to stop Cube (in this function we never want)

        //Apart from translation and rotation, we update smoothTime, starting with a high value so we get a smooth movement
        //in the X-Axis

        //UPDATING SMOOTH TIME

        gameObject.Tween("Updating smooth time", initialSmoothTime, cameraMovement.GetDefaultSmoothTime(), time, TweenScaleFunctions.Linear, (t) =>
        {
            //Progress
            cameraMovement.smoothTime = t.CurrentValue;
        }, (t) =>
         {
             //Completion
         });

        if (ct != null)
        {
            //UPDATING LENS SETTINGS

            updateLensSettings(ct.FocusDistance, ct.Aperture, ct.FocalLength, time);
        }
    }

    void rotateCamera (float startAngle, float targetAngle, float time, bool stopCube)
    {
        gameObject.Tween("Rotate camera", startAngle, targetAngle, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            transform.rotation = Quaternion.Euler(t.CurrentValue, 0, 0);
        }, (t) =>
        {
            //Completion
            if (stopCube) { cubeMovement.enabled = true; }
        });
    }

    void updateLensSettings (float targetFocusDistance, float targetAperture, float targetFocalLength, float time)
    {
        PostProcessingProfile ppp = GetComponent<PostProcessingBehaviour>().profile;

        float startFocusDistance = ppp.depthOfField.settings.focusDistance;
        DepthOfFieldModel.Settings depthOfField = ppp.depthOfField.settings;

        gameObject.Tween("Updating focus distance", startFocusDistance, targetFocusDistance, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            depthOfField.focusDistance = t.CurrentValue;
            ppp.depthOfField.settings = depthOfField;
            
        }, (t) =>
        {
            //Completion
        });

        float startAperture = ppp.depthOfField.settings.aperture;

        gameObject.Tween("Updating aperture", startAperture, targetAperture, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            depthOfField.aperture = t.CurrentValue;
            ppp.depthOfField.settings = depthOfField;

        }, (t) =>
        {
            //Completion
        });

        float startFocalLength = ppp.depthOfField.settings.focalLength;

        gameObject.Tween("Updating aperture", startFocalLength, targetFocalLength, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            depthOfField.focalLength = t.CurrentValue;
            ppp.depthOfField.settings = depthOfField;

        }, (t) =>
        {
            //Completion
        });        
    }

    void OnApplicationQuit()
    {
        resetDepthOfFieldValues();
    }

    void resetDepthOfFieldValues()
    {
        DepthOfFieldModel.Settings depthOfField = GetComponent<PostProcessingBehaviour>().profile.depthOfField.settings;

        depthOfField.focusDistance = default_focusDistance;
        depthOfField.aperture = default_aperture;
        depthOfField.focalLength = default_focalLength;

        GetComponent<PostProcessingBehaviour>().profile.depthOfField.settings = depthOfField;
    }
}
