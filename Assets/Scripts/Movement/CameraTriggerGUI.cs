﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CameraTrigger))]
public class CameraTrigerGUI : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CameraTrigger myCheckpoint = (CameraTrigger)target;
        if (GUILayout.Button("Get Lens state"))
        {
            myCheckpoint.GetLensState();
        }
    }
}
