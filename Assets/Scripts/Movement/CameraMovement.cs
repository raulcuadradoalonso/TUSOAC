﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraMovement : MonoBehaviour {

    public Transform target;
    public float deepness;
    public float smoothTime;
    public float height;

    const float DEFAULT_ROTATION = 9.8f;
    const float DEFAULT_UPDATING_TIME = 1.5f; //Time to update the position of the camera
    float DEFAULT_SMOOTH_TIME;

    bool move = true;
    bool stop_everyting = false;

    GameObject lastCameraPositionTrigger; //We track the last trigger so we do not 'reupdate' the position unnecesarily

    CameraTween ct;
    Camera cam;
    
    [HideInInspector] public Vector3 updatedPosition;
    [HideInInspector] public Vector3 velocity = Vector3.zero;

	void Start ()
    {
        ct = GetComponent<CameraTween>();
        cam = GetComponent<Camera>();
        DEFAULT_SMOOTH_TIME = smoothTime;
	}

    void FixedUpdate()
    {        
        if (!stop_everyting)
        {
            Vector3 targetPosition = new Vector3(target.position.x, transform.position.y, transform.position.z);
            updatedPosition = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);

            if (move)
            {
                transform.position = updatedPosition;
            }
        }           
    }
    //To update camera view
    public void UpdateCameraView(Collider other)
    {
        if (other.tag == "CameraPosition" && lastCameraPositionTrigger != other.transform.parent.gameObject)
        {
            lastCameraPositionTrigger = other.transform.parent.gameObject;
            PostProcessingBehaviour ppb = lastCameraPositionTrigger.GetComponent<PostProcessingBehaviour>();

            if (ct.cameraStatic)
            {
                //Debug.Log("From static to resume movement");
                ct.FromStaticToResumeMovement(lastCameraPositionTrigger.transform.position,
                    lastCameraPositionTrigger.transform.rotation.eulerAngles.x, DEFAULT_UPDATING_TIME, 0.5f, lastCameraPositionTrigger.GetComponent<CameraTrigger>());
            }
            else if (lastCameraPositionTrigger.GetComponent<CameraTrigger>().moveToStaticPosition)
            {
                //Debug.Log("Move camera to static position");
                ct.MoveCameraToStaticPosition(lastCameraPositionTrigger.transform.position,
                    lastCameraPositionTrigger.transform.rotation.eulerAngles.x, DEFAULT_UPDATING_TIME, lastCameraPositionTrigger.GetComponent<CameraTrigger>().stopCube, lastCameraPositionTrigger.GetComponent<CameraTrigger>());
            }
            else if (!lastCameraPositionTrigger.GetComponent<CameraTrigger>().moveToStaticPosition)
            {
                //Debug.Log("Update camera view");
                ct.UpdateCameraView(lastCameraPositionTrigger.transform.position,
                    lastCameraPositionTrigger.transform.rotation.eulerAngles.x, DEFAULT_UPDATING_TIME, lastCameraPositionTrigger.GetComponent<CameraTrigger>());
            }            
        }
    }

    public void Stop()
    {        
        move = false;
    }

    public void Resume()
    {
        move = true;
    }

    public void StopEverything()
    {
        stop_everyting = true;
        Stop();
        ResetVelocity();
    }

    public void ResumeEverything()
    {
        stop_everyting = false;
        Resume();
    }

    public void ResetVelocity()
    {
        velocity = Vector3.zero;
    }

    public bool IsMoving()
    {
        return move;
    }

    public bool IsStopped()
    {
        return stop_everyting;
    }

    public void ResetSmoothTime()
    {
        smoothTime = 0.1f;
    }

    public void ResetDefaultValues()
    {
        deepness = -8.5f;
        smoothTime = 0.1f;
        height = 2.5f;
    }

    public float GetDefaultSmoothTime()
    {
        return DEFAULT_SMOOTH_TIME;
    }

    public void resetTriggerTracking()
    {
        lastCameraPositionTrigger = null;
    }
}
