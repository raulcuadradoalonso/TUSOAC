﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    [Header("References")]
    public Text NewGameText;
    public Text LanguageText;
    public Text ExitText;
    public Text TitleText;
    public LocalizationManager lm;
    public Image fader;
    public EventSystem eSystem;
    [Header("Available languages")]
    public string[] Languages;

    int languageCounter = 0;

    void Start()
    {
        eSystem.SetSelectedGameObject(NewGameText.transform.parent.gameObject);
        fader.gameObject.SetActive(true);
        fader.canvasRenderer.SetAlpha(0);        
    }

    void Update()
    {
        if (lm == null)
        {
            lm = GameObject.Find("LocalizationManager").GetComponent<LocalizationManager>();
        }

        TitleText.text = lm.GetMainMenuDictionary()["title"];
        NewGameText.text = lm.GetMainMenuDictionary()["newgame"];
        LanguageText.text = lm.GetMainMenuDictionary()["language"];
        ExitText.text = lm.GetMainMenuDictionary()["exit"];
    }

    public void ChangeLanguage()
    {
        languageCounter++;

        if (languageCounter > Languages.Length - 1)
        {
            languageCounter = 0;
        }

        lm.SetLanguage(Languages[languageCounter]);
    }

    public void StartNewGame(int index)
    {
        fader.raycastTarget = true;
        StartCoroutine(fadeAndLoadScene(index));
    }

    IEnumerator fadeAndLoadScene(int index)
    {
        fader.CrossFadeAlpha(1, 1, true);
        yield return new WaitForSeconds(1.5f);
        LoadSceneByIndex(index);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    public void LoadSceneByIndex(int index)
    {
        SceneManager.LoadScene(index);
    }
}
