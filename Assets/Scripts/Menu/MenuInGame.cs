﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuInGame : MonoBehaviour
{

    public PlayerDialog pd;

    public EventSystem eventSystem;

    public Image Fader;
    public Button ResumeButton;
    public Button ExitButton;

    bool menuActive = false;
	
	void Update ()
    {
		if ((Input.GetKeyDown(KeyCode.Joystick1Button7) || Input.GetKeyDown(KeyCode.Escape)))
        {
            if (!menuActive && !pd.IsInteracting)
            {
                menuActive = true;
                pd.IsInteracting = true;
                showMenu();
            }
            else if (menuActive)
            {
                menuActive = false;
                pd.IsInteracting = false;
                hideMenu();
            }
        }
	}

    void showMenu()
    {
        GetComponent<Canvas>().enabled = true;
        eventSystem.SetSelectedGameObject(ResumeButton.gameObject);
        AkSoundEngine.PostEvent("InGameMenuActive", this.gameObject);
    }

    void hideMenu()
    {
        GetComponent<Canvas>().enabled = false;
        AkSoundEngine.PostEvent("InGameMenuInactive", this.gameObject);
    }

    public void ResumeGame()
    {
        hideMenu();
        menuActive = false;
        pd.IsInteracting = false;
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);
        AkSoundEngine.PostEvent("StopEverything", this.gameObject);
    }
}
