﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Checkpoint))]
public class CheckpointSetEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Checkpoint myCheckpoint = (Checkpoint)target;

        if (GUILayout.Button("Activate Checkpoint"))
        {
            myCheckpoint.ActivateCheckpoint();
        }

        if (GUILayout.Button("Set up Checkpoint"))
        {
            myCheckpoint.SetUpCheckpoint();
        }        
    }
}
