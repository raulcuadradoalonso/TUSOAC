﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class Checkpoint : MonoBehaviour {

    public Vector3 PlayerPosition;
    public Quaternion PlayerRotation;
    public Vector3 CameraPosition;
    public Quaternion CameraRotation;
    //Lens settings
    public float FocusDistance;
    public float Aperture;
    public float FocalLength;
    //Auxiliar profile for lens settings
    DepthOfFieldModel.Settings depthOfField;

    public void ActivateCheckpoint()
    {
        Debug.Log("Activating checkpoint");
        //Setting up player position and rotation
        GameObject.Find("CubePlayer").transform.position = PlayerPosition;
        GameObject.Find("CubePlayer").transform.rotation = PlayerRotation;
        //Setting up camera position and rotation
        Camera.main.transform.position = CameraPosition;
        Camera.main.transform.rotation = CameraRotation;
        //Updating post-processing stack
        //Creating settings for depth of field
        depthOfField.aperture = Aperture;
        depthOfField.focalLength = FocalLength;
        depthOfField.focusDistance = FocusDistance;
        Camera.main.GetComponent<PostProcessingBehaviour>().profile.depthOfField.settings = depthOfField;
    }

    public void SetUpCheckpoint()
    {
        Debug.Log("Setting up checkpoint");
        //Setting up default player position and rotation
        PlayerPosition = GameObject.Find("CubePlayer").transform.position;
        PlayerRotation = GameObject.Find("CubePlayer").transform.rotation;
        //Setting up camera position and rotation
        CameraPosition = Camera.main.transform.position;
        CameraRotation = Camera.main.transform.rotation;
        //Storing post-processing current state
        PostProcessingProfile ppp = Camera.main.GetComponent<PostProcessingBehaviour>().profile;
        FocusDistance = ppp.depthOfField.settings.focusDistance;
        Aperture = ppp.depthOfField.settings.aperture;
        FocalLength = ppp.depthOfField.settings.focalLength;        
    }
}
