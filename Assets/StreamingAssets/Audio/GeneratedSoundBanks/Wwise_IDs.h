/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AERIALCAMERA = 1820391438U;
        static const AkUniqueID APARTMENTOST = 924933933U;
        static const AkUniqueID CARMOVING = 231575315U;
        static const AkUniqueID CLOSEFRIDGE = 3407863996U;
        static const AkUniqueID CLOSEMETALLICGRID = 2965642936U;
        static const AkUniqueID DISABLEAERIALEFFECT = 235716118U;
        static const AkUniqueID DOOROPEN = 1404805401U;
        static const AkUniqueID ELEVATORCLOSE = 2109880937U;
        static const AkUniqueID ELEVATORDING = 2543937455U;
        static const AkUniqueID ELEVATORMOVES = 1306411967U;
        static const AkUniqueID ELEVATOROPEN = 2554485435U;
        static const AkUniqueID EXITPIPELINE = 2221622947U;
        static const AkUniqueID INGAMEMENUACTIVE = 3561574529U;
        static const AkUniqueID INGAMEMENUINACTIVE = 1462315628U;
        static const AkUniqueID KNOCKDOOR = 2850110183U;
        static const AkUniqueID LANDCAMERA = 3211808491U;
        static const AkUniqueID MISTERYOST = 1472854570U;
        static const AkUniqueID NIGHTIME = 259715552U;
        static const AkUniqueID OPENFRIDGE = 2911132066U;
        static const AkUniqueID OPENGATES = 1995398023U;
        static const AkUniqueID OPENMETALLICGRID = 4162401294U;
        static const AkUniqueID PRESSBUTTON = 2561485778U;
        static const AkUniqueID RAIN_SOUND = 2722794375U;
        static const AkUniqueID RELEASEBUTTON = 3174762640U;
        static const AkUniqueID SOMEONEKNOCKS = 1470211200U;
        static const AkUniqueID STARTDARKAMBIENCE = 3644257135U;
        static const AkUniqueID STOPELEVATOR = 772106695U;
        static const AkUniqueID STOPEVERYTHING = 1940514798U;
        static const AkUniqueID STOPMUSIC = 1917263390U;
        static const AkUniqueID TV_SOUND = 2155969695U;
        static const AkUniqueID TYPEKEYBOARD = 2432663154U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID SOUNDFX = 2810670744U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID AERIALSOUNDFX = 135377602U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
